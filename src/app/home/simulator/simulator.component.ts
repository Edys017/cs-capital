import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ServicesModule } from 'src/app/services/services.module';


@Component({
  selector: 'app-simulator',
  templateUrl: './simulator.component.html',
  styleUrls: ['./simulator.component.scss']
})
export class SimulatorComponent implements OnInit {

  selection: number = 1;

  csPayYear: number = 0;
  csPayMonth: number = 0;
  bankValue: number = 0;
  totalYear: number = 0;
  totalMonth: number = 0;
  totalBank: number = 0;
  monthInterest: number = 0;
  yearInterest: number = 0;

  investment = new FormGroup ({
    value: new FormControl(1000, Validators.required),
  })

  constructor(private api: ServicesModule) { }

  ngOnInit(): void {
    this.calculateInvestment();
  }

  contactWhatsapp() {
    window.open('https://api.whatsapp.com/send?phone=' +
    '593978812149' + '&text=' + 'Hola. Me interesa invertir en CS Capital.');
  }

  calculateInvestment() {
    console.log('MONTO', this.investment.get('value').value )
    const body = {
      monto: this.investment.get('value').value
    }

    if(this.investment.get('value').value >= 500 && this.investment.get('value').value < 1000000000000 ) {
      this.api.simulateInvestment(body).subscribe(
        result => {
          console.log('RESULTADO',result)
          this.csPayMonth = result['pagoMensual'];
          this.csPayYear = result['pagoAnual'];
          this.bankValue = result['pagoOtroBanco'];
          this.totalMonth = result['totalMensual'];
          this.totalYear = result['totalAnual'];
          this.totalBank = result['totalOtroBanco'];
          this.monthInterest = result['interesMensual'];
          this.yearInterest = result['interesAnual'];
          // document.getElementById('plan-1-container').style.border='2px solid rgba(60, 60, 60, 1)'
          document.getElementById('plan-1-container').style.border='2px solid #606060'
          // box-shadow: (0 0 5px rgba(81, 203, 238, 1));
          document.getElementById('plan-2-container').style.border='2px solid #606060'
          // document.getElementById('monto').style.pointer-events='none';
          setTimeout(function() {
            document.getElementById('plan-1-container').style.border='none'
            document.getElementById('plan-2-container').style.border='none'
          }, 2000);
        }, 
        error => {
          console.log(error)
        }
      )
    } else {
          this.csPayMonth = 0;
          this.csPayYear = 0;
          this.bankValue = 0;
          this.totalMonth = 0;
          this.totalYear = 0;
          this.totalBank = 0;
          this.monthInterest = 0;
          this.yearInterest = 0;
    }
  }


}
