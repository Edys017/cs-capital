import { Component, OnInit } from '@angular/core';
import Swiper from 'swiper/bundle';
import SwiperCore, { Navigation, Pagination } from 'swiper/core';
SwiperCore.use([Navigation, Pagination]);
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ServicesModule } from '../services/services.module';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  panelOpenState = false;

  form1 = new FormGroup ({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('', [Validators.maxLength(10),Validators.minLength(7), Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]),
    message: new FormControl('', [Validators.maxLength(150), Validators.required])
  })

  projects: Array <any> = [
    {
      image: './../../assets/general/project-3.webp',
      title: 'Deja un legado',
      description: 'Invierte en tu futuro y en el de aquellos que más amas.'
    },
    {
      image: './../../assets/general/project-2.webp',
      title: 'El mundo a tu alcance',
      description: 'Viaja a los destinos que siempre quisiste conocer.'
    },
    {
      image: './../../assets/general/project-1.webp',
      title: 'Los proyectos más ambiciosos',
      description: 'Obtén el capital para ese negocio que tanto has anhelado.'
    }
  ]

  cs_info: Array <any> = [
    {
      title: 'CS Capital',
      content: `CS Capital es un proyecto ecuatoriano con el objetivo de ofrecer una opción segura y altamente rentable a las personas que buscan invertir su dinero y generar ingresos extras.`
    },
    {
      title: 'Cómo lo hacemos',
      content: `La rentabilidad pagada a los inversionistas se genera en mercados bursátiles como NASDAQ Y US30. 
      Tenemos experiencia y preparación en trading, con resultados confirmados y exitosos. Nosotros invertimos por ti.`
    },
  ]

  warranty: Array <any> = [
    {
      image: './../../assets/information/Money.svg',
      content: 'Deposita tu capital a una cuenta bancaria nacional y respalda tu inversión.'
    },
    {
      image: './../../assets/information/ShieldCheck.svg',
      content: 'Contrato, con seguro de capital. Aseguramos la devolución de tu inversión.'
    },
    {
      image: './../../assets/information/Feature Icon with circle.svg',
      content: 'Si no pagamos tu rentabilidad a tiempo, recibirás un pago adicional.'
    },
  ]

  inversores: Array <any> = [
    {
      nombre: 'Jissela Arcos Molina ',
      cargo: 'Ingeniera en telecomunicaciones / empresaria',
      imagen: './../../assets/investers/invester_1.webp',
      opinion: 'Mi esposo y yo habíamos ahorrado dinero para comprarnos un terreno; pero, gracias a Dios conocimos CS Capital e invertimos nuestro dinero ahí. Por su seriedad y puntualidad hemos realizado varias reinversiones. Tenemos la plena confianza que nuestro capital está seguro y hemos podido cumplir más metas con el residual que recibimos cada mes. Con ese dinero pudimos adquirir un carro y estamos ahorrando para poder no solo comprarnos un terreno, si no también construir nuestra casa. Estamos eternamente agradecidos con Cris y Edith.',
      phrase : 'Una bendición en nuestra vida'
    },
    {
      nombre: 'Flavio Arias Pilatuña',
      cargo: 'Docente Educación básica / Escuela superior de la policía ',
      imagen: './../../assets/investers/invester_2.webp',
      opinion: 'Invertir en CS CAPITAL realmente ha sido un éxito total ya que he logrado mejorar mi estilo de vida y cumplir metas a corto plazo junto con mi familia con los buenos réditos que ha producido mi capital invertido.',
      phrase : 'Un éxito total'
    },
  ]

  general_info: Array <any> = [
    {
      numero: 1743,
      descripcion: 'Rentabilidades pagadas',
      imagen: './../../assets/images/main-logo.png',
    },
    {
      numero: 87,
      descripcion: 'Planes anuales',
      imagen: './../../assets/images/main-logo.png',
    },
    {
      numero: 65,
      descripcion: 'Planes mensuales',
      imagen: './../../assets/images/main-logo.png',
    },

  ]

  steps: Array <any> = [
    {
      img: './../../assets/steps/CheckSquareOffset.webp',
      title: 'Elige el plan',
      description: 'El que más te convenga: mensual o anual.'
    },
    {
      img: './../../assets/steps/NotePencil.webp',
      title: 'Elaboramos el contrato',
      description: 'Lo revisas y apruebas. Garantizamos tu inversión.'
    },
    {
      img: './../../assets/steps/CurrencyCircleDollar.webp',
      title: 'Realizas el depósito',
      description: 'Seguro y directo a la cuenta bancaria.'
    },
    {
      img: './../../assets/steps/ChartLineUp.webp',
      title: 'Listo',
      description: 'Recibes tu rentabilidad mensual o anual.'
    }
  ]

  questions1: Array <any> = [
    {
      question: '¿Cuál es el monto mínimo para invertir en CS CAPITAL?',
      answer: 'CS CAPITAL recibe inversiones desde $500 para rentabilidad anual o mensual.'
    },
    {
      question: '¿Qué duración tienen los contratos?',
      answer: 'Los contratos tienen una duración de un año calendario (365 días).'
    },
    {
      question: '¿Puedo perder mi inversión?',
      answer: 'Toda inversión tiene un riesgo sin embargo gracias al conocimiento en mercados financieros de los gestores, CS CAPITAL se responsabiliza del total de tu capital invertido.'
    }
  ]

  questions2: Array <any> = [
    {
      question: '¿Cuál es el porcentaje de rentabilidad de una inversión?',
      answer: 'La rentabilidad va desde un 3% al 6% sobre tu capital. Da click en este enlace para simular tu inversión y saber exactamente cuanto ganarías.'
    },
    {
      question: '¿Cómo se genera la rentabilidad para los inversionistas?',
      answer: `Los bancos, entidades financieras y traders ganan a través de operaciones en los mercados como FOREX, ÍNDICES BURSÁTILES como NASDAQ y US30.
      Con el conocimiento apropiado de trading y el capital, generamos ganancias constantes para pagar a nuestros inversionistas.`
    },
    {
      question: '¿Cómo invierto en CS CAPITAL?',
      answer: 'Eliges tu plan si es mensual o anual, suscribimos tu contrato, realizas el depósito y listo tu dinero está trabajando para ti.'
    }
  ]
  
  constructor(private api: ServicesModule) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    const swiper = new Swiper('.swiper-container', {
      
      slidesPerView: 2,
      spaceBetween: 30,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      loop: true,
      speed: 1000,
      autoplay: {
        delay: 2500,
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
        },
        600: {
          slidesPerView: 2,
        }
      }
    });

  }

  toggleQuestion(id) {
    var question = document.getElementById("question" + id);
    if (question.style.maxHeight) {
      question.style.maxHeight = null;
    } else {
      question.style.maxHeight = question.scrollHeight + "px";
    }
  }

  submitForm() {
    console.log('FORMULARIO', this.form1)
    const body = {
      nombre: this.form1.get('name').value,
      correo: this.form1.get('email').value,
      telefono: this.form1.get('phone').value,
      mensaje: this.form1.get('message').value,
    }

    if(this.form1.valid) {
      this.api.sendEmail(body).subscribe(
        result => {
          console.log('CORREO ENVIADO',result)
          document.getElementById('mensaje-correo').style.display = 'block'
        }, 
        error => {
          console.log(error)
        }
      )
    }
  }

  contactWhatsapp() {
    window.open('https://api.whatsapp.com/send?phone=' +
    '593978812149' + '&text=' + 'Hola. Me interesa invertir en CS Capital.');
  }

}
