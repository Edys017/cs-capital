import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import { throwError } from 'rxjs';
import {catchError} from 'rxjs/operators';





@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ServicesModule { 
  
  // ipPublica = 'https://pruebas.ushops.tech';
  ipPublica = 'https://api.cscapitalinvest.com';

  baseUser = this.ipPublica +  '/csc-usuarios';

  constructor(private http: HttpClient) { }

  simulateInvestment(params): Observable<any>{
    return this.http.patch(`${this.baseUser}/simulador`, params,{}).pipe(catchError( this.handleError));
  }
  sendEmail(params): Observable<any>{
    return this.http.patch(`${this.baseUser}/email`, params,{}).pipe(catchError( this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('error:', error.error);
    } else {
      if (error.status === 401) {
        console.error('no autorizado:');
      }
    }
    return throwError(error.error);
  }

}
