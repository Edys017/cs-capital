import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FlexLayoutModule } from '@angular/flex-layout';

// import {MatRadioModule} from '@angular/material/radio';




@NgModule({
  declarations: [HeaderComponent, FooterComponent],
  exports: [
    HeaderComponent,
    FooterComponent,
    // MatRadioModule
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    // MatRadioModule
  ]
})
export class SharedModule { }
