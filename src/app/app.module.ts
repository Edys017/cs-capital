import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './home/home.component';
import { SimulatorComponent } from './home/simulator/simulator.component';

import { FlexLayoutModule } from '@angular/flex-layout';

import {MatRadioModule} from '@angular/material/radio';

import { ServicesModule } from './services/services.module';

import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import {MatExpansionModule} from '@angular/material/expansion';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SimulatorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    FlexLayoutModule,

    MatRadioModule,
    MatExpansionModule,

    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  // exports: [
  //   MatRadioModule
  // ],
  providers: [ServicesModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
